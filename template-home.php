<?php
/*
Template Name: Home Template
*/
?>

<div class="slider-wrapper theme-sunyit">
	<div id="slider" class="nivoSlider">
		<?php if( $images = get_field('carousel') ): ?>
			<?php foreach( $images as $n => $image ): ?>
				<?php if ( $link = get_field('link',$image['id']) ): ?>
					<a href="<?php echo $link ?>"><img src="<?php echo $image['url'] ?>"></a>
				<?php else: ?>
					<img src="<?php echo $image['url'] ?>">
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').nivoSlider({
					effect: 'fade',
					pauseTime: 5000
				});
		});
	</script>
</div>

<div class="row">

	<div class="col-lg-6 col-lg-push-3 col-md-6 col-md-push-3 col-sm-12 home-news">
		
		<div class="row">
		
			<div class="col-lg-6 col-md-6 col-sm-6 home-news-releases home-module home-module--news-releases">
		
				<h2 class="h4 home-module__heading section-header"><a href="<?php echo get_field('news_releases_url') ?>">News Releases</a></h2>

				<?php //do_shortcode('[feed url="http://www.sunyit.edu/apps/blogs/news/category/news/feed" num"2"]'); ?>
				<?php
				$feed = fetch_feed_with_options(array(
					'url' => get_field('news_releases_feed'), //'http://www.sunyit.edu/apps/blogs/news/category/featured/feed',
					'num' => 2
				));
		
				foreach ($feed as $item): ?>
					<article class="summary summary--home">
						<h3 class="h5" class="summary__heading"><a href="<?php echo $item->get_link(); ?>"><?php echo shorten( $item->get_title(), 56); ?></a></h3>
						<p class="summary__content"><?php echo shorten( $item->get_description(), 150 ); ?></p>
						<p class="summary__more text-right"><strong><a href="<?php echo $item->get_link(); ?>">Read More</a> <span class="accent-color">&raquo;</span></strong></p>
					</article>
				<?php endforeach; ?>

			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 home-news-aboutus home-module home-module--news-about-us">
		
				<h2 class="h4 home-module__heading section-header"><a href="<?php echo get_field('news_about_us_url') ?>">News About Us</a></h2>

				<?php //do_shortcode('[feed url="http://www.sunyit.edu/apps/blogs/news/category/news/feed" num"2"]'); ?>
				<?php
				$feed = fetch_feed_with_options(array(
					'url' => get_field('news_about_us_feed'), //'http://www.sunyit.edu/apps/blogs/news/category/news/feed',
					'num' => 2
				));
		
				foreach ($feed as $item): ?>
					<article class="summary summary--home">
						<h3 class="h5 summary__heading"><a href="<?php echo $item->get_link(); ?>"><?php echo shorten( $item->get_title(), 56); ?></a></h3>
						<p class="summary__content"><?php echo shorten( $item->get_description(), 150 ); ?></p>
						<p class="summary__more text-right"><strong><a href="<?php echo $item->get_link(); ?>">Read More</a> <span class="accent-color">&raquo;</span></strong></p>
					</article>
				<?php endforeach; ?>

			</div>
		
		</div>
		
		<h2 class="h3"><a href="/virtualtour">Take a Virtual Tour</a></h2>
		
		<!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>--><!-- optional -->
		<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/vendor/jquery.accordionza.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {

				$('#virtual-tour').accordionza({
            autoPlay: false,
            autoRestartDelay: 0,
            pauseOnHover: true,
            slideDelay: 3000,
            //slideEasing: 'easeOutCirc',
            slideSpeed: 400,
            slideTrigger: 'mouseover',
            startSlide: 1
				});

		});
		</script>

		<?php 

		$images = get_field('virtual_tour');

		if( $images ): ?>
			<ul id="virtual-tour">
					<?php foreach( $images as $n => $image ): ?>
						<li class="slide<?php echo $n+1 ?>">
							<p class="slide_handle"><?php echo $image['title'] ?></p>
							<p class="slide_content">
								<?php if ( $link = get_field('link',$image['id']) ): ?>
									<a href="<?php echo $link ?>"><img src="<?php echo $image['url'] ?>"></a>
								<?php else: ?>
									<img src="<?php echo $image['url'] ?>">
								<?php endif; ?>
							</p>
						</li>
					<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		
		<br>
		
	</div>

	<div class="col-lg-3 col-lg-pull-6 col-md-3 col-md-pull-6 col-sm-6 home-events home-module home-module--events">
		<h2 class="h4 section-header home-module__heading"><a href="/events">Events</a></h2>

		<?php
		date_default_timezone_set('America/New_York');
		$feed = fetch_feed_with_options(array(
			'url' => 'http://www.sunyit.edu/apps/calendar/export.php?type=rss&sponsortype=all&timebegin=today',
			'num' => 3,
			'simplepie_enable_order_by_date' => false
		));
		
		foreach ($feed as $item): ?>
			<article>
				<h3 class="h6">
					<big><span class="event-date accent-color"><?php echo date("n/j",strtotime($item->get_date())) ?></span></big>
					<span class="p"><a href="<?php echo $item->get_permalink(); ?>"><?php echo $item->get_title(); ?></a></span>
				</h3>
				<!--<div><?php echo $item->get_description(); ?></div>-->
			</article>
		<?php endforeach; ?>
		
		<br>
		
		<?php do_shortcode('[sunyit_vtcalendar url="http://www.sunyit.edu/apps/calendar/export.php?type=rss&sponsortype=all&timebegin=today"]') ?>

		<br>

		<p class="text-right"><strong><a href="http://sunyit.edu/calendars_events/academic_calendars">Academic Calendars</a> <span class="accent-color">&raquo;</span></strong></p>
		
	</div>

	<div class="col-lg-3 col-md-3 col-sm-6 home-announcements home-module home-module--announcements">

		<section class="home-announcements">
			<h2 class="h4 section-header home-module__heading">Announcements</h2>

			<ul class="list-inline text-center">
				<li class="h4"><a href="http://www.sunyit.edu/apps/blogs/news/category/student-announcements/">Students</a></li>
				<li class="h4"><a href="http://www.sunyit.edu/apps/blogs/news/category/faculty-announcements/">Faculty</a></li>
				<li class="h4"><a href="http://www.sunyit.edu/apps/blogs/news/category/staff-announcements/">Staff</a></li>
			</ul>
			
			<div class="home-quicklinks">
			
				<h2 class="h4">Interested in SUNY Poly?</h2>
				<ul class="custom-bullets">
					<li><a href="/admissions/visit">Visit Campus</a></li>
					<li><a href="/catalogs">Apply Now</a></li>
					<li><a href="/registrar/instructions">Register Now</a></li>
					<li><a href="/virtualtour">Take a Virtual Tour</a></li>
					<li><a href="http://www.sunycnse.com/">Our Albany NanoTech Campus</a></li>
				</ul>

				<h2 class="h4">Need more info?</h2>
				<ul class="custom-bullets">
					<li><a href="/about">About Us</a></li>
					<li><a href="/directions">Directions / Maps</a></li>
					<li><a href="/employment">Employment</a></li>
					<li><a href="/cpe_k_12">K-12 Outreach</a></li>
					<li><a href="/virtualtour">Visitors</a></li>
				</ul>

			</div>
				
			<br>

		</section>
			
		<section class="text-center">

			<img src="http://sunyit.edu/images/USnewsSUNYITpublic.png" height="101" width="99">&nbsp;&nbsp;
			<img src="http://sunyit.edu/images/military_friendly_sunyit.png" height="78" width="116">

		</section>

	</div>

</div>

