<header class="banner" role="banner">

  <div class="container">

    <nav id="primary-navigation" class="navbar navbar-sunyit navbar-static-top yamm" role="navigation">
			
			<div class="navbar-header">
				<button class="navbar-btn btn btn-primary btn-outline btn-lg" data-toggle="collapse" data-target=".navbar-collapse">
					Menu
				</button>

        <?php if ( get_theme_mod('nesco_logo') ): ?>
          <a class="navbar-brand" href="<?php echo home_url(); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
            <img src="<?php echo esc_url( get_theme_mod( 'nesco_logo' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
          </a>
        <?php else: ?>
          <a class="navbar-brand" href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
        <?php endif; ?>
				
			</div>
    
      <div class="collapse navbar-collapse">

				<div class="navbar-sidebar">
					<?php dynamic_sidebar('sidebar-header'); ?>
				</div>

        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
          endif;
        ?>
      </div>

    </nav>

  </div>

</header>
