<?php
/**
 * Custom functions
 */

function bootstrap_breadcrumbs() {
	
	if (is_front_page()) {
		if (!is_multisite()) return;
		if (is_multisite() && is_main_site()) return;
	}
		
	echo '<ol class="breadcrumb">';
	
	if (!is_multisite() || (is_multisite() && is_main_site())) {
		echo '<li><a href="'.get_option('home').'">Home</a>';
	} else {
		echo '<li><a href="'.network_home_url().'">Home</a>';
		if (is_front_page()) {
			echo '<li>'.get_bloginfo('name');
		} else {
			echo '<li><a href="'.get_option('home').'">'.get_bloginfo('name').'</a>';
		}
	}

	if (!is_front_page()) {

		if (is_single()) {
			$post = get_post();

			/*
			if ( is_custom_post_type($post) ) {
				echo '<li>';
				$post_type = get_post_type_object($post->post_type);
				echo $post_type->label;
			}
			*/
			
			$categories = get_the_category( $post->ID );
			if ( count($categories) > 0 ) {
				echo '<li>';
				the_category(', ');
			}
			
			echo '<li>';
			the_title();
			
		} elseif (is_category()) {
			echo '<li>';
			single_cat_title();
		} elseif (is_page() && (!is_front_page())) {
			$post = get_post();
			$parents = get_post_ancestors( $post->ID );
			foreach (array_reverse($parents) as $parent_id) {
				$parent = get_page($parent_id);
				echo '<li>';
				echo '<a href="' . get_permalink($parent_id) . '">';
				echo $parent->post_title;
				echo '</a>';
			}
			echo '<li>';
			the_title();
		} elseif (is_tag()) {
			echo '<li>Tag: ';
			single_tag_title();
		} elseif (is_day()) {
			echo'<li>Archive for ';
			the_time('F jS, Y');
		} elseif (is_month()) {
			echo'<li>Archive for ';
			the_time('F, Y');
		} elseif (is_year()) {
			echo'<li>Archive for ';
			the_time('Y');
		} elseif (is_author()) {
			echo'<li>Author Archives';
		} elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
			echo '<li>Blog Archives';
		} elseif (is_search()) {
			echo'<li>Search Results';
		}
	}
	echo '</ol>';
}

/**
 * Check if a post is a custom post type.
 * http://wordpress.stackexchange.com/questions/6731/if-is-custom-post-type
 * @param  mixed $post Post object or ID
 * @return boolean
 */
function is_custom_post_type( $post = NULL )
{
    $all_custom_post_types = get_post_types( array ( '_builtin' => FALSE ) );

    // there are no custom post types
    if ( empty ( $all_custom_post_types ) )
        return FALSE;

    $custom_types      = array_keys( $all_custom_post_types );
    $current_post_type = get_post_type( $post );

    // could not detect current type
    if ( ! $current_post_type )
        return FALSE;

    return in_array( $current_post_type, $custom_types );
}

function sunyit_widgets_init() {

  register_sidebar(array(
    'name'          => __('Header', 'roots'),
    'id'            => 'sidebar-header',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));

  register_sidebar(array(
    'name'          => __('Footer Secondary', 'roots'),
    'id'            => 'sidebar-footer-secondary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));
}

add_action('widgets_init', 'sunyit_widgets_init');


// disable inline CSS styles for image galleries
add_filter( 'use_default_gallery_style', '__return_false' );


/**
 * http://premium.wpmudev.org/blog/daily-tip-how-to-embed-rss-feed-entries-in-wordpress-posts/
 */

include_once(ABSPATH.WPINC.'/class-feed.php');

function fetch_feed_with_options($options=array()) {

	extract(shortcode_atts(array(
		"url" => 'http://',
		"num"  => '10',
	), $options));

	add_filter( 'wp_feed_cache_transient_lifetime', 'get_homepage_news_feed_lifetime' );
	$feed = fetch_feed($url);
	remove_filter( 'wp_feed_cache_transient_lifetime', 'get_homepage_news_feed_lifetime' );
  
  if (!(is_object($feed) && get_class($feed)==='SimplePie')) {
    return false;
  }
  
	foreach ($options as $key => $val) {
		if (strpos($key,'simplepie_') === 0) {
			$func = substr($key,10);
			$feed->$func($val);
		}
	}
	
	return $feed->get_items(0,$num);
	
}
add_shortcode('feed', 'fetch_feed_with_options');

function get_homepage_news_feed_lifetime( $seconds ) {
	return 900; // 15 minutes
}

function nivo_slider_assets () {
	wp_enqueue_script( 'nivo_slider_js',    get_template_directory_uri() . '/assets/vendor/nivo-slider/jquery.nivo.slider.pack.js', array( 'jquery' ), '3.2', true );
  wp_enqueue_style(  'nivo_slider_css',   get_template_directory_uri() . '/assets/vendor/nivo-slider/nivo-slider.css', false, '3.2');
  wp_enqueue_style(  'nivo_slider_theme', get_template_directory_uri() . '/assets/lib/nivo-slider-sunyit-theme/sunyit.css', false, '3.2');	
}
add_action( 'wp_enqueue_scripts', 'nivo_slider_assets' );

function sunyit_theme_assets () {
	wp_enqueue_script( 'jquery_hoverintent',			 get_template_directory_uri() . '/assets/js/vendor/jquery.hoverIntent.minified.js', array( 'jquery' ), '1.8.0', true );
	wp_enqueue_script( 'bootstrap_hover_dropdown', get_template_directory_uri() . '/assets/js/vendor/bootstrap-hover-dropdown.js', array( 'jquery' ), '2.0.11', true );
}
add_action( 'wp_enqueue_scripts', 'sunyit_theme_assets' );

function shorten($string, $length)
{
		
		if (strlen($string) <= $length) {
			return $string;
		}
		
    // By default, an ellipsis will be appended to the end of the text.
    $suffix = '&hellip;';
 
    // Convert 'smart' punctuation to 'dumb' punctuation, strip the HTML tags,
    // and convert all tabs and line-break characters to single spaces.
    $short_desc = trim(str_replace(array("\r","\n", "\t"), ' ', strip_tags($string)));
 
    // Cut the string to the requested length, and strip any extraneous spaces 
    // from the beginning and end.
    // $desc = trim(substr($short_desc, 0, $length));

    // Cut the string to the requested length, splitting on word boundaries,
    // and strip any extraneous spaces from the beginning and end.
		$desc = current( explode("\n", wordwrap(trim($short_desc), $length, "\n") ));
 
    // Find out what the last displayed character is in the shortened string
    $lastchar = substr($desc, -1, 1);
 
    // If the last character is a period, an exclamation point, or a question 
    // mark, clear out the appended text.
    if ($lastchar == '.' || $lastchar == '!' || $lastchar == '?') $suffix='';
 
    // Append the text.
    $desc .= $suffix;
 
    // Send the new description back to the page.
    return $desc;
}

function nesco_theme_customizer( $wp_customize ) {

  $wp_customize->add_section( 'nesco_logo_section', array(
    'title'       => __( 'Logo', 'nesco' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
  ));
  $wp_customize->add_setting( 'nesco_logo' );
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'nesco_logo', array(
    'label'    => __( 'Logo', 'nesco' ),
    'section'  => 'nesco_logo_section',
    'settings' => 'nesco_logo',
) ) );

}
add_action('customize_register', 'nesco_theme_customizer');
